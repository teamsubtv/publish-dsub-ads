package controllers;

import play.*;
import play.mvc.*;
import play.data.*;
import play.data.validation.*;
import play.data.validation.Constraints.*;
import static play.data.Form.*;
import views.html.*;
import views.html.login;
import views.html.publish;
import views.html.cancelAds;
import models.*;
import javax.validation.*;
import javax.persistence.Entity;
import java.lang.Integer;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.Date;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.math.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Application extends Controller {

    /* Render pages */
    public static Result login() {
        return ok(login.render(form(Login.class)));
    }

    public static Result logout() {
        session().clear();
        flash("success", "You've been logged out");
        return redirect(routes.Application.login());
    }

    //@Security.Authenticated(Secured.class)
    public static Result publish() {
        return ok(publish.render(form(Upload.class), ""));
    }

    public static Result freeSlots() {
        Map<String, String> map = new HashMap<String, String>();
        map = getTable();
        return ok(freeSlots.render(map));
    }

    public static Result cancelAds() {
        ArrayList<String> temp = new ArrayList<String>();
        return ok(cancelAds.render(form(cancelAdChannelID.class), temp));
    }


    /* Methods */
    // Connect to the database, get free slot, upload the data and return with an error message ("" means no error)
    public static String connectAndUploadToDatabase(Upload data) {
        Connection conn = null;
        Statement stmt;
        List<String> takenSlots = new ArrayList<String>();
        Map<String, String> map = new HashMap<String, String>();
        String displayErrorMessage = "";
        String errorMessage = "NO FREE SLOTS";
        String stringChannels = data.channelId;
        String[] listChannels = stringChannels.split(",");
        int listSize = listChannels.length;
        int maxSlots = 5;

        try {
            Class.forName(ConnectionInfo.driver);
            conn = DriverManager.getConnection(ConnectionInfo.url, ConnectionInfo.userName, ConnectionInfo.password);
            // Execute query and check slots for each channel. Add the channel ID and a free slot to a map.
            for (int i = 0; i < listSize; i++) {
                String query = "SELECT * FROM [dbo].[T_DSUBADDS] where channel = " + listChannels[i] +
                        " and startdate <= getdate() and enddate >= getdate() order by slot;";
                stmt = conn.createStatement();

                // Add any taken slots to takenSlots
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    String slot = rs.getString("SLOT");
                    takenSlots.add(slot);
                }

                /*
                    If takenSlots doesn't contain a certain integer, add the channel ID and the integer to the map,
                    else add the error message
                */
                if (!takenSlots.contains("1")) {
                    map.put(listChannels[i], "1");
                } else if (!takenSlots.contains("2")) {
                    map.put(listChannels[i], "2");
                } else if (!takenSlots.contains("3")) {
                    map.put(listChannels[i], "3");
                } else if (!takenSlots.contains("4")) {
                    map.put(listChannels[i], "4");
                } else if (!takenSlots.contains("5")) {
                    map.put(listChannels[i], "5");
                } else {
                    map.put(listChannels[i], errorMessage);
                }

                stmt.close();
            }

            // Add the channel ID to displayErrorMessage (error message) that will be diplayed when the user tries to submit the form
            for (int i = 0; i < listSize; i++) {
                String channel = listChannels[i];
                String slot = map.get(channel);
                if (slot.equals(errorMessage)) {
                    displayErrorMessage += channel + " has no free slots";
                }
            }

            // Get values given by the user and insert data to the table
            String fileName = "'" + data.fileName + "'";
            String format = "'" + data.fileFormat + "'";
            String advertiser = "'" + data.advertiser + "'";
            String startDate = "'" + data.startDate + "'";
            String endDate = "'" + data.endDate + "'";
            String INSYNC = "'0'";
            String SYNCDELETE = "'1'";
            String SYNCUPDATE = "'1'";
            String columns = "fileName,format,channel,advertiser,startDate,endDate,INSYNC,SYNCDELETE,SYNCUPDATE,SLOT";

            // Loop through the given channel IDs and if the channel ID has at least 1 slot, add data to the database
            for (int i = 0; i < listSize; i++) {
                String channel = listChannels[i];
                String slot = map.get(channel);

                if (!slot.equals(errorMessage)) {
                String wholeData = fileName + "," + format + "," + channel + "," + advertiser + "," + startDate + "," + endDate + "," + INSYNC + "," + SYNCDELETE + "," + SYNCUPDATE + "," + slot;
                String queryUploadData = "INSERT INTO T_DSUBADDS (" + columns + ") VALUES (" + wholeData + ")";
                stmt = conn.createStatement();
                stmt.executeUpdate(queryUploadData);
                stmt.close();
                }
            }

            conn.close();
            return displayErrorMessage;

        }   catch(Exception e){
            e.printStackTrace();
        }
        return displayErrorMessage;
    }

    // Pass an array which contains information about a given channel ID to the cancelAds template
    public static Result displayChannelIdInfo() {
        Connection conn = null;
        ArrayList<String> array = new ArrayList<String>();
        Form<cancelAdChannelID> cancelAdForm = form(cancelAdChannelID.class).bindFromRequest();
        cancelAdChannelID data = cancelAdForm.get();
        String channelID = data.channelID;

        try {
            Class.forName(ConnectionInfo.driver);
            conn = DriverManager.getConnection(ConnectionInfo.url, ConnectionInfo.userName, ConnectionInfo.password);
            Statement stmt = conn.createStatement();
            String query = "SELECT * FROM [dbo].[T_DSUBADDS] where channel = " + channelID +
                    " and startdate <= getdate() and enddate >= getdate() order by slot;";

            if(!channelID.equals("")) {
                ResultSet rs = stmt.executeQuery(query);

                // Add items from the database to array
                while (rs.next()) {
                    array.add(rs.getString("id"));
                    array.add(rs.getString("fileName"));
                    array.add(rs.getString("channel"));
                    array.add(rs.getString("advertiser"));
                    array.add(rs.getString("SLOT"));
                    array.add(rs.getString("format"));
                    array.add(rs.getString("startDate"));
                    array.add(rs.getString("endDate"));
                    array.add(rs.getString("INSYNC"));
                    array.add(rs.getString("SYNCDELETE"));
                    array.add(rs.getString("SYNCUPDATE"));
                }
            }

            return ok(cancelAds.render(form(cancelAdChannelID.class), array));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ok(cancelAds.render(form(cancelAdChannelID.class), array));
    }

    // Cancel an advert by changing a value in the table then return to the publish page
    public static Result cancelAdvert() {
        Connection conn = null;
        Form<cancelAdChannelID> cancelAdForm = form(cancelAdChannelID.class).bindFromRequest();
        cancelAdChannelID data = cancelAdForm.get();
        String id = data.ID;

        try {
            Class.forName(ConnectionInfo.driver);
            conn = DriverManager.getConnection(ConnectionInfo.url, ConnectionInfo.userName, ConnectionInfo.password);

            // Delete an advert that has the given ID
            Statement stmt = conn.createStatement();
            if(!id.equals("")) {
                // Execute query to change the value of column SYNCDELETE to 0 to cancel an advert
                String query = "UPDATE [dbo].[T_DSUBADDS] " + "SET SYNCDELETE = 0 WHERE id in ("+ id + ")";
                int rs = stmt.executeUpdate(query);

            }

            stmt.close();

            return ok(publish.render(form(Upload.class), ""));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ok(publish.render(form(Upload.class), ""));
    }

    // Add venue information (channel ID, free slots) to a map and return the map which will be used in freeSlots template
    public static Map getTable() {
        Connection conn = null;
        Statement stmt;
        Map<String, String> map = new HashMap<String, String>();
        int takenSlotsSize = 0;
        int maxSlots = 5;
        int listSize;
        String freeSlots;

        try {
            Class.forName(ConnectionInfo.driver);
            conn = DriverManager.getConnection(ConnectionInfo.url, ConnectionInfo.userName, ConnectionInfo.password);

            String[] listChannelIDs = new String[]{"477", "545", "455", "541", "421", "393", "454", "10586",
                    "51", "187", "433", "402", "537", "10595", "395", "10553", "508", "425", "10560", "10559", "10596",
                    "547", "431", "10602"};

            /*
                Execute query and check slots for each channel. Add the channel ID and a free slot to a map. If there
                are no free slots for a particular channel ID, add an appropriate string
            */
            listSize = listChannelIDs.length;
            for (int i = 0; i < listSize; i++) {
                String query = "SELECT * FROM [dbo].[T_DSUBADDS] where channel = " + listChannelIDs[i] +
                        " and startdate <= getdate() and enddate >= getdate() order by slot;";
                List<String> takenSlots = new ArrayList<String>();
                freeSlots = "";
                stmt = conn.createStatement();

                ResultSet rs = stmt.executeQuery(query);

                // Calculate how many free slots are available. Each slot will be added to a takenSlots array
                while (rs.next()) {
                    String slot = rs.getString("SLOT");
                    takenSlots.add(slot);
                }

                /*
                    Loop maxSlots times to find out any free slots and add them to a string which is then added to a map
                    along with the channel ID.
                */
                for(int x = 1; x <= maxSlots; x++) {
                    String slot = Integer.toString(x);
                    if (!takenSlots.contains(slot)) {
                        freeSlots += "," + slot;
                    }
                }

                // If takenSlots size has 5 (maxSlots) items in it then there are no free slots. Add a message
                if (takenSlots.size() == maxSlots) {
                    freeSlots += ",No free slots";
                }

                // Add the channel ID and corresponding free slots
                map.put(listChannelIDs[i], freeSlots);
                stmt.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;

    }


    /* Classes */
    // Used by other methods in this file to gain database access information
    public static class ConnectionInfo {
        public static String url = "jdbc:sqlserver://10.147.46.4;databaseName=SPARK";
        public static String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        public static String userName = "sa";
        public static String password = "0308Pr0sper";
    }

    // Used by the login form
    public static class Login {
        public String email = "";
        public String password = "";

        public String validate() {
            if (User.authenticate(email, password) == null) {
                return "Invalid user or password";
            }
            return null;
        }
    }

    // Used by the publishing form
    public static class Upload {
        public String filePath;
        public String venue;
        public String channelId;
        public String fileName;
        public String advertiser;
        public String startDate;
        public String endDate;
        public String fileFormat;
    }

    // Used by the canceling advert form
    public static class cancelAdChannelID {
        public String channelID;
        public String ID;
    }


    /* Authentication and validation methods */
    // Authenticate login page
    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(login.render(loginForm));
        } else {
            session().clear();
            session("email", loginForm.get().email);
            return redirect(routes.Application.publish());
        }
    }

    // Validate publish page
    public static Result validateForm() {
        Form<Upload> publishForm = form(Upload.class).bindFromRequest();
        Upload data = publishForm.get();

        /*
           If connectAndUploadToDatabase() returns an error, it will be displayed on the website. It will also upload
           channel IDs that have free slots. If an empty string is returned, no error was found and the data will be
           added to the database
        */
        String error = connectAndUploadToDatabase(data);
        return ok(publish.render(form(Upload.class), error));
    }
}