package models;

import play.data.validation.*;
import play.db.ebean.*;
import com.avaje.ebean.*;
import javax.persistence.*;
import java.io.File;
import java.util.Date;

@Entity
public class Publish extends Model {

    @Id
    public String fileName;
    public String filePath;
    public String venue;
    public String channelId;
    public String advertiser;
    public String startDate;
    public String endDate;
    public String fileFormat;

    public Publish(String filePath, String venue, String channelId, String fileName, String advertiser, String startDate, String endDate, String fileFormat) {
        this.filePath = filePath;
        this.venue = venue;
        this.channelId = channelId;
        this.fileName = fileName;
        this.advertiser = advertiser;
        this.startDate = startDate;
        this.endDate = endDate;
        this.fileFormat = fileFormat;

    }

    public static Finder<String, Publish> find = new Finder<String, Publish>(
            String.class, Publish.class
    );
}