# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table publish (
  file_name                 varchar(255) not null,
  file_path                 varchar(255),
  venue                     varchar(255),
  channel_id                varchar(255),
  advertiser                varchar(255),
  start_date                varchar(255),
  end_date                  varchar(255),
  file_format               varchar(255),
  constraint pk_publish primary key (file_name))
;

create table user (
  email                     varchar(255) not null,
  password                  varchar(255),
  constraint pk_user primary key (email))
;

create sequence publish_seq;

create sequence user_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists publish;

drop table if exists user;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists publish_seq;

drop sequence if exists user_seq;

